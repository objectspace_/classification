import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeClassifier, export_graphviz
import os
import subprocess

def visualize_tree(tree, feature_names):
    """Create tree png using graphviz.

    Args
    ----
    tree -- scikit-learn DecsisionTree.
    feature_names -- list of feature names.
    """
    with open("dt.dot", 'w') as f:
        export_graphviz(tree, out_file=f,
                        feature_names=feature_names)

    command = ["dot", "-Tpng", "dt.dot", "-o", "dt.png"]
    try:
        subprocess.check_call(command)
    except:
        exit("Could not run dot, ie graphviz, to "
             "produce visualization")

def decision_tree():
    data = pd.read_csv('data-transformed.csv')
    y = data['MM_ID_T']
    x = data[['TYPE_T', 'CURCY_CD_T', 'EXCH_CD_T', 'LIMIT_T']]
    dt = DecisionTreeClassifier(min_samples_split=20, random_state=99)
    return dt.fit(x,y)

if __name__ == '__main__':
    tree = decision_tree()
    visualize_tree(tree, ['TYPE_T', 'CURCY_CD_T', 'EXCH_CD_T', 'LIMIT_T'])