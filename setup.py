from distutils.core import setup

setup(
    name='Classification',
    version='1.0',
    packages=['pandas', 'scikit-learn'],
    url='',
    license='Creative Commons',
    author='Zuber Saiyed',
    author_email='',
    description=''
)
