from sklearn import preprocessing
import pandas as pd


def leppard():
    good_responses = [
        1183,
        2522,
        1868,
        456,
        767,
        1084,
        2110,
        1083,
        2191,
        929,
        1078,
        709,
        473,
        723,
        2194,
        1025,
        1024]

    mainDf = pd.read_csv('data.csv')
    type_array = transform(mainDf, 'TYPE')
    curr_array = transform(mainDf, 'CURCY_CD')
    exch_array = transform(mainDf, 'EXCH_CD')
    mmid_array = transform(mainDf, 'MM_ID')
    mainDf['TYPE_T'] = type_array
    mainDf['CURCY_CD_T'] = curr_array
    mainDf['EXCH_CD_T'] = exch_array
    mainDf['MM_ID_T'] = mmid_array

    mainDf['LIMIT_T'] = mainDf['LIMIT'].apply(lambda x: 1 if pd.notnull(x) else 0)

    mainDf.drop(labels=['TYPE', 'CURCY_CD', 'EXCH_CD', 'MM_ID', 'LIMIT'], axis=1, inplace=True)
    print(mainDf)
    mainDf.to_csv('data-transformed.csv', index=False)


def transform(df, column):
    encoder = preprocessing.LabelEncoder()
    encoder = encoder.fit(df[column])
    # print(encoder.classes_)
    return encoder.fit_transform(df[column])


if __name__ == '__main__':
    leppard()
